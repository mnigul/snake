package snake2;

public class Finals {
	/*
	 * Maaran suundadele int vaartused ja deklareerin kastide ja ruudustiku
	 * suuruse.
	 */
	public static final int NO_DIRECTION = 0;
	public static final int UP = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	public static final int RIGHT = 4;

	public static final int BOX_HEIGHT = 15;
	public static final int BOX_WIDTH = 15;
	public static final int GRID_HEIGHT = 25;
	public static final int GRID_WIDTH = 25;

}