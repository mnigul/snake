package snake2;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.LinkedList;

public class SnakeCanvas extends Canvas implements Runnable, KeyListener {

	/*
	 * Deklareerin ussi punktidest linkedlistina.
	 */
	public static LinkedList<Point> snakeBody;
	private Food fruit = new Food();

	private Thread startThread;
	private int score = 0;

	private Image menuPicture = null;
	private boolean menu = true;

	private boolean endGame = false;
	private boolean won = false;

	public static int direction = Finals.NO_DIRECTION;

	/*
	 * Maarab suuruse, linked listi, genereerib ussi, asetab toidu ja teeb
	 * graafika.
	 */
	public void paint(Graphics g) {
		/*
		 * Kaivitatakse thread.
		 */
		if (startThread == null) {
			this.setPreferredSize(new Dimension(640, 480));
			this.addKeyListener(this);
			startThread = new Thread(this);
			startThread.start();
		}
		/*
		 * Menyys olles joonistatakse menyy, mangulopu ekraanil olles
		 * joonistatakse lopuekraan, kui pole kummaski, siis joonistatakse koik
		 * muu.
		 */
		if (menu) {
			drawMenu(g);
		} else if (endGame) {
			drawEnd(g);
		} else {
			if (snakeBody == null) {
				snakeBody = new LinkedList<Point>();
				generateSnake();
				fruit.placeFood();
			}
			/*
			 * joonistan koik elemendid canvasele, kasutades eelnevad graphics
			 * objekte.
			 */
			fruit.drawFood(g);
			drawGrid(g);
			drawSnake(g);
			drawScore(g);
		}
	}

	/*
	 * Genereerib mangu lopus teate, vastavalt kas kaotusele voi voidule.
	 */
	public void drawEnd(Graphics g) {
		BufferedImage endGameImage = new BufferedImage(this.getPreferredSize().width, this.getPreferredSize().height,
				BufferedImage.TYPE_INT_ARGB);
		Graphics endGameGraphics = endGameImage.getGraphics();
		endGameGraphics.setColor(Color.MAGENTA);
		if (won) {
			endGameGraphics.drawString("Imekombel suutsid ara voita, vinge!", this.getPreferredSize().width / 2,
					this.getPreferredSize().height / 2);
		} else {
			endGameGraphics.drawString("Kahjuks sa kaotasid, aga proovi uuesti!", this.getPreferredSize().width / 2,
					this.getPreferredSize().height / 2);
		}
		endGameGraphics.drawString("Sinu skoor: " + this.score, this.getPreferredSize().width / 2,
				(this.getPreferredSize().height / 2) + 20);
		endGameGraphics.drawString("Vajuta \"SPACE\", et uuesti alustada.", this.getPreferredSize().width / 2,
				(this.getPreferredSize().height / 2) + 40);
		g.drawImage(endGameImage, 0, 0, this);
	}

	public void drawMenu(Graphics g) {
		if (this.menuPicture == null) {
			/*
			 * Laen pildi systeemist.
			 */
			try {
				URL imagePath = SnakeCanvas.class.getResource("snakeMenu.png");
				this.menuPicture = Toolkit.getDefaultToolkit().getImage(imagePath);
			} catch (Exception e) {
				/*
				 * Pilti ei eksisteeri.
				 */
				e.printStackTrace();
			}
		}
		g.drawImage(menuPicture, 0, 0, 640, 480, this);
	}

	/*
	 * See on vaikimisi update meetod, milles on double buffering repaint
	 * meetodi jaoks.
	 */
	public void update(Graphics g) {
		/*
		 * Graafika, mis luuakse tagataustal.
		 */
		Graphics backgroundImageGraphics;
		BufferedImage backgroundImage = null;
		Dimension d = this.getSize();

		backgroundImage = new BufferedImage(d.width, d.height, BufferedImage.TYPE_INT_ARGB);
		backgroundImageGraphics = backgroundImage.getGraphics();
		backgroundImageGraphics.setColor(this.getBackground());
		backgroundImageGraphics.fillRect(0, 0, d.width, d.height);
		backgroundImageGraphics.setColor(this.getForeground());
		paint(backgroundImageGraphics);

		g.drawImage(backgroundImage, 0, 0, this);
	}

	/*
	 * Genereerin 3 osaga ussi ilma suunata ja maaran skoori algvaartuse.
	 */
	public void generateSnake() {
		score = 0;
		snakeBody.clear();
		snakeBody.add(new Point(0, 2));
		snakeBody.add(new Point(0, 1));
		snakeBody.add(new Point(0, 0));
		direction = Finals.NO_DIRECTION;
	}
	/*
	 * Koik liikumisega seotud.
	 */
	public void move() {
		if (direction == Finals.NO_DIRECTION) {
			return;
		}
		/*
		 * Maaran head-i esimeseks listi osaks, määran ara, kuidas uss liigub
		 * igas suunas, votab viimase osa ara, lisab yhe ette.
		 */
		Point head = snakeBody.peekFirst();
		Point newPoint = head;
		switch (direction) {
		case Finals.UP:
			newPoint = new Point(head.x, head.y - 1);
			break;
		case Finals.DOWN:
			newPoint = new Point(head.x, head.y + 1);
			break;
		case Finals.LEFT:
			newPoint = new Point(head.x - 1, head.y);
			break;
		case Finals.RIGHT:
			newPoint = new Point(head.x + 1, head.y);
			break;
		}
		/*
		 * Enne if bloki kaivitumist, eemaldan ussi viimase lyli.
		 */
		if (this.direction != Finals.NO_DIRECTION) {
			snakeBody.remove(snakeBody.peekLast());
		}
		/*
		 * Kui ussi pea jouab toidu peale, vastavalt mis suunas uss liigub,
		 * lisab pea oigesse kohta.
		 */
		if (newPoint.equals(fruit.food)) {
			score += 10;
			Point addPoint = (Point) newPoint.clone();
	
			switch (direction) {
			case Finals.UP:
				newPoint = new Point(head.x, head.y - 1);
				break;
			case Finals.DOWN:
				newPoint = new Point(head.x, head.y + 1);
				break;
			case Finals.LEFT:
				newPoint = new Point(head.x - 1, head.y);
				break;
			case Finals.RIGHT:
				newPoint = new Point(head.x + 1, head.y);
				break;
			}
			snakeBody.push(addPoint);
			fruit.placeFood();

		}
		/*
		 * Uss läks x-telje piiridest valja, siis mang algab uuesti.
		 */
		else if (newPoint.x < 0 || newPoint.x > (Finals.GRID_WIDTH - 1)) {
			won = false;
			endGame = true;
			return;

		}
		/*
		 * Uss läks y-telje piiridest valja, siis mang algab uuesti.
		 */
		else if (newPoint.y < 0 || newPoint.y > (Finals.GRID_HEIGHT - 1)) {
			won = false;
			endGame = true;
			return;

		}
		/*
		 * Uss sõidab iseendasse, siis mang algab uuesti.
		 */
		else if (snakeBody.contains(newPoint)) {
			if (direction != Finals.NO_DIRECTION) {
				won = false;
				endGame = true;
				return;
			}
		}
		/*
		 * Kui uss täidab kõik kastid, siis mang on voidetud.
		 */
		else if (snakeBody.size() == (Finals.GRID_WIDTH * Finals.GRID_HEIGHT)) {
			won = true;
			endGame = true;
			return;
		}
		/*
		 * Lisab uue punkti ussi juurde.
		 */
		snakeBody.push(newPoint);
	}

	public void drawGrid(Graphics g) {
		/*
		 * Joonistan välise kasti.
		 */
		g.drawRect(0, 0, Finals.GRID_WIDTH * Finals.BOX_WIDTH, Finals.GRID_HEIGHT * Finals.BOX_HEIGHT);
		/*
		 * joonistan verikaalsed jooned.
		 */
		for (int x = Finals.BOX_WIDTH; x < Finals.GRID_WIDTH * Finals.BOX_WIDTH; x += Finals.BOX_WIDTH) {
			g.drawLine(x, 0, x, Finals.BOX_HEIGHT * Finals.GRID_HEIGHT);
		}
		/*
		 * Joonistan horisontaalsed jooned.
		 */
		for (int y = Finals.BOX_HEIGHT; y < Finals.GRID_HEIGHT * Finals.BOX_HEIGHT; y += Finals.BOX_HEIGHT) {
			g.drawLine(0, y, Finals.GRID_WIDTH * Finals.BOX_WIDTH, y);
		}
	}
	/*
	 * Joonistatakse skoor ekraanile.
	 */
	public void drawScore(Graphics g) {
		g.drawString("Skoor: " + score, 0, Finals.BOX_HEIGHT * Finals.GRID_HEIGHT + 10);
	}
	/*
	 * Joonistatakse uss.
	 */
	public void drawSnake(Graphics g) {
		g.setColor(Color.GREEN);
		for (Point p : snakeBody) {
			g.fillRect(p.x * Finals.BOX_WIDTH, p.y * Finals.BOX_HEIGHT, Finals.BOX_WIDTH, Finals.BOX_HEIGHT);
		}
		g.setColor(Color.BLACK);
	}

	@Override
	public void run() {
		/*
		 * See loop töötab koguaeg, paneb mängu sujuvalt tööle.
		 */
		while (true) {
			repaint();
			if (!menu && !endGame) {
				move();
			}
			/*
			 * Buffer, 10 FPS.
			 */
			try {
				Thread.currentThread();
				Thread.sleep(100);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Koik nuppudega seotud loogika.
	 */
	@Override
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			if (direction != Finals.DOWN) {
				direction = Finals.UP;
			}
			break;
		case KeyEvent.VK_DOWN:
			if (direction != Finals.UP) {
				direction = Finals.DOWN;
			}
			break;
		case KeyEvent.VK_RIGHT:
			if (direction != Finals.LEFT) {
				direction = Finals.RIGHT;
			}
			break;
		case KeyEvent.VK_LEFT:
			if (direction != Finals.RIGHT) {
				direction = Finals.LEFT;
			}
			break;
		case KeyEvent.VK_ENTER:
			if (menu) {
				menu = false;
				repaint();
			}
			break;
		case KeyEvent.VK_ESCAPE:
			menu = true;
			break;
		case KeyEvent.VK_SPACE:
			if (endGame) {
				endGame = false;
				won = false;
				generateSnake();
				repaint();
			}
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}