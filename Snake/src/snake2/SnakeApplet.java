package snake2;

import java.applet.Applet;
import java.awt.Dimension;

/*
 * Kaivitab programmi appletis.
 */
public class SnakeApplet extends Applet {

	private SnakeCanvas c;

	public void init() {
		c = new SnakeCanvas();
		c.setPreferredSize(new Dimension(640, 480));
		c.setVisible(true);
		c.setFocusable(true);
		this.add(c);
		this.setVisible(true);
		this.setSize(new Dimension(640, 480));
	}
}